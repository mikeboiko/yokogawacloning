#!/usr/bin/env python
"""
Clone yokogawa configuration/graphics
"""

import argparse
import os
import re
from pathlib import Path

import yaml


def parse_args():
    "Parse CLI arguments"

    # Title
    parser = argparse.ArgumentParser(__doc__)

    # Positional arguments
    parser.add_argument(dest='dict_file', action='store', help='Name of dictionary yaml file')
    parser.add_argument(
        dest='input_file', action='store', help='Name of original text file, where replacements are to occur'
    )

    # Root Folder
    parser.add_argument(
        '-r',
        '--root-folder',
        dest='root_folder',
        action='store',
        help='Name of relative folder path where to output',
    )

    # Optional arguments
    parser.add_argument(
        '-o',
        '--output-folder',
        dest='output_folder',
        action='store',
        default='output',
        help='Name of relative folder path where to output',
    )

    # Boolean arguments
    parser.add_argument(
        '--latin1',
        dest='latin1',
        action='store_true',
        help='Specify if encoding is latin1 (default is utf-8)',
    )
    parser.add_argument(
        '--stack-files',
        dest='stack_files',
        action='store_true',
        help='Stack all output files into combined file',
    )
    parser.add_argument(
        '--gen-graphic', dest='gen_graphic', action='store_true', help='Re-number generic graphic sets'
    )

    return parser.parse_args()


class Clone:
    """
    Cloning all the things
    """

    def __init__(self):
        """
        Initialize all the variables
        """

        # Each clone contains a bunch of replacements and config
        self.clones = {}

        # This should be handled for better sometime
        self.newline = '\r\n'

    def run(self):
        "Run the script"

        self.args = parse_args()

        # Specify the encoding used
        if self.args.latin1:
            self.encoding = 'latin-1'
        else:
            self.encoding = 'utf-8'

        # Read input text file
        self.read_input_file()

        # Perform find/replace
        self.load_replacements()
        self.perform_replacements()

        # Stack files
        if self.args.stack_files or self.args.gen_graphic:
            self.stack_files()

        # Update set numbers for generic graphics
        if self.args.gen_graphic:
            self.renumber_generic_graphic_sets()

    def find_and_replace(self, replacements):
        "Perform find/replace and save new file"

        # Create output file directory if it doesn't exist
        Path(self.output_file.parent).mkdir(parents=True, exist_ok=True)

        # Open output text file for writing
        with open(self.output_file, 'w', encoding=self.encoding, newline=self.newline) as outfile:
            # Loop through each line of input file
            # TODO-MB [200918] - not sure if I need to consider \r\n sometimes
            for line in self.input_file_text.strip('\n').split('\n'):
                # This forces use of copy instead of pointer.
                replaced_line = line

                # Loop through each dictionary item
                for key, value in replacements.items():
                    # Perform regular expression substitution
                    if 'lambda' in value:
                        replaced_line = re.sub(key, eval(value), replaced_line)
                    else:
                        replaced_line = re.sub(key, value, replaced_line)

                # Write new line to output file
                outfile.write(replaced_line + '\n')
        print(f'File saved: {self.output_file}')

    def load_replacements(self):
        """
        Load replacements from yaml file
        """

        # Dictionary replacements file
        dict_file = Path.cwd().joinpath(self.args.dict_file)

        # Load yaml file into dict
        config = self.load_yaml(dict_file)

        # Initialize sets in clones with empty dicts
        for x in range(
            config.get('sets').get('first'),
            config.get('sets').get('last') + 1,
            config.get('sets').get('step'),
        ):
            self.clones[x] = {'variables': {}, 'substitutions': {}}

        # Iterate variables and substitutions
        self.parse_variables(config)
        self.parse_substitutions(config)

    def load_yaml(self, fileName):
        """
        Load configuration from yaml file
        """

        # Try reading yaml file
        try:
            with open(fileName, 'r') as yamlFile:
                config = yaml.load(yamlFile, Loader=yaml.FullLoader)
        except OSError as e:
            print(e)
            return

        return config

    def read_input_file(self):
        """
        Read input text file
        handle for generic graphics
        """

        self.input_file_text = Path(self.args.input_file).read_text(encoding=self.encoding)
        if self.args.gen_graphic:
            header = '#SetNumber,SetName,Comment,IsDefaultSet,,'
            self.input_file_text = header + self.input_file_text.split(header)[1]

    def parse_substitutions(self, config):
        "Iterate over each pattern and parse lists/dicts"

        # Loop through each clone
        for k1, v1 in self.clones.items():
            # Create replacements
            for repl_type, repl_items in config.get('substitutions').items():
                for k2, v2 in repl_items.items():
                    # Replace all the clone-set variable inside {}
                    for k3, v3 in v1.get('variables').items():
                        # zfill for keys such as zfill-2, zfill-3, etc...
                        repl_string = str(v3).zfill(int(repl_type[-1])) if 'zfill' in repl_type else str(v3)
                        v2 = str(v2).replace('{' + k3 + '}', repl_string)
                    # Add to clones dict
                    self.clones[k1]['substitutions'][str(k2)] = v2

    def parse_variables(self, config):
        "Iterate over each pattern and parse lists/dicts"

        # Loop over each variable type and add to clones
        for key, value in config.get('variables').items():
            # Create iterator of clones so dict can be refered to by an index sort-of
            clone_iterator = iter(self.clones)

            # Manual lists
            if type(value) is list:
                iterate_over = value

            # Sequential patterns - dictionaries
            elif type(value) is dict:
                iterate_over = range(
                    value.get('start'),
                    value.get('start') + len(self.clones) * value.get('step') + 1,
                    value.get('step'),
                )

            # Constant
            else:
                iterate_over = [value] * len(self.clones)

            # Loop through pattern range
            for x in iterate_over:
                # Don't go through th rest of the set, if there's an error
                try:
                    clone_index = next(clone_iterator)
                except:
                    break

                # Add to master clones dict
                self.clones[clone_index]['variables'][key] = x

    def perform_replacements(self):
        "Perform dictionary replacements for each clone"

        root_folder = (
            Path(self.args.root_folder)
            if self.args.root_folder
            else Path(self.args.input_file).resolve().parents[0]
        )

        # Perform dictionary replacements
        for key, value in self.clones.items():
            # The output_file name is the input filename with the clone key appended
            self.output_file = (
                root_folder.joinpath(self.args.output_folder)
                .joinpath()
                .joinpath(
                    Path(self.args.input_file).resolve().stem
                    + '-'
                    + str(key)
                    + Path(self.args.input_file).resolve().suffix
                )
            )

            self.find_and_replace(value['substitutions'])

            # Add output_file to dictionary
            self.clones[key]['output_file'] = str(self.output_file)

    def renumber_generic_graphic_sets(self):
        "Renumber generic graphic sets"

        # The combined generic graphic file name
        gen_graphic_file = (
            Path(self.args.input_file)
            .resolve()
            .parents[0]
            .joinpath(self.args.output_folder)
            .joinpath()
            .joinpath(Path(self.args.input_file).resolve().stem + Path(self.args.input_file).resolve().suffix)
        )

        # Create iterator of clones so next() can be called
        clone_iterator = iter(self.clones)

        # Replace all set info after #SetNumber is found
        with open(stacked_files_name, 'r', newline=self.newline) as infile:
            with open(gen_graphic_file, 'w') as outfile:
                for line in infile:
                    outfile.write(line)
                    if '#SetNumber' in line:
                        # Go to next set
                        clone = next(clone_iterator)

                        # Get next line and perform substing replace
                        line = next(infile).replace('"1"', f'"{clone}"')
                        outfile.write(line)

        # Delete stacked file because this one overrides it
        os.remove(stacked_files_name)

    def stack_files(self):
        "Combine all the text files into one big file"

        global stacked_files_name

        # The combined files
        stacked_files_name = (
            Path(self.args.input_file)
            .resolve()
            .parents[0]
            .joinpath(self.args.output_folder)
            .joinpath()
            .joinpath(
                Path(self.args.input_file).resolve().stem
                + '_stacked'
                + Path(self.args.input_file).resolve().suffix
            )
        )

        # Create list of files (to be joined)
        stacked_files = [clone['output_file'] for clone in self.clones.values()]

        # Stack the files
        with open(stacked_files_name, 'w', newline=self.newline) as outfile:
            for graphic in stacked_files:
                with open(graphic, 'r') as infile:
                    outfile.write(infile.read())
                os.remove(graphic)
        print(f'File saved: {stacked_files_name}')


# Run script if this file is executed directly
if __name__ == '__main__':
    Clone().run()
