#!/usr/bin/env bash

# Input file
filename=tags
fileext=csv

# Well pads 3-1, 3-2 and 4-2 only have 11 pairs
cp /home/mike/git/Yokogawa/Cloning/replacements/WellPairs.yaml /tmp/WellPairs.yaml
sed -i 's/last: 12/last: 11/' /tmp/WellPairs.yaml

# Clone pads
python ~/git/Yokogawa/Cloning/clone.py \
    /home/mike/git/Yokogawa/Cloning/replacements/WellPads.yaml \
    $filename.$fileext

# Clone 11 pairs
for x in 5 6 8
do
    # cat output/$filename-$x.$fileext
    python ~/git/Yokogawa/Cloning/clone.py \
        /tmp/WellPairs.yaml \
        output/$filename-$x.$fileext \
        --stack-files
done

# Clone 12 pairs
for x in 1 2 3 4 7 9 10 11
do
    # cat output/$filename-$x.$fileext
    python ~/git/Yokogawa/Cloning/clone.py \
        /home/mike/git/Yokogawa/Cloning/replacements/WellPairs.yaml \
        output/$filename-$x.$fileext \
        --stack-files
done

# Combine into one file
cat output/output/tags-*_stacked.csv > tags-combined.csv
