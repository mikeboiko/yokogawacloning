#   ____ _     ___  _   _ _____
#  / ___| |   / _ \| \ | | ____|
# | |   | |  | | | |  \| |  _|
# | |___| |__| |_| | |\  | |___
#  \____|_____\___/|_| \_|_____|


# After exporting generic graphics, make sure to delete everything after the first set
# Place the input/output files into Input/ and Output/ because they are in .gitignore

# input=./Input/testInput.txt
# outputDir=Output/OTSG
# pipenv run python ./clone.py ./Replacements/OTSG.yaml $input -o ./Output/OTSG
# vim -d $input $outputDir/testInput10.txt

python clone.py ./replacements/WP.yaml ./data-output/FCS0424_DR0162.txt -o ./data-output/wp
exit

folder=/media/HIS0364/RS-Projects/ABP/SCS0330/SCS0330/SCS0330
pipenv run python ../clone.py ../data-input/ABP.yaml $folder/P_SIS_CLONE.STF -o $folder

# folder=/media/HIS0264l/temp/cloning
# pipenv run python ../clone.py ../data-input/ABP.yaml $folder/202L4I041T.xaml -o $folder/out
# pipenv run python ../clone.py ../data-input/ABP.yaml $folder/202L4I042T.xaml -o $folder/out
# pipenv run python ../clone.py ../data-input/ABP.yaml $folder/202L4I043T.xaml -o $folder/out
# pipenv run python ../clone.py ../data-input/ABP.yaml $folder/FCS0317a.txt -o $folder/out
# pipenv run python ../clone.py ../data-input/ABP.yaml $folder/FCS0317b.txt -o $folder/out
# pipenv run python ../clone.py ../data-input/ABP.yaml $folder/202LC01043.txt -o $folder/out

pipenv run python ./clone.py ./Replacements/OTSG.yaml ./Input/drawing.txt -o ./Output/OTSG
pipenv run python ./clone.py ./Replacements/OTSG.yaml ./Input/switches.csv -o ./Output/OTSG --stack-files
pipenv run python ./clone.py ./Replacements/OTSG.yaml ./Input/tuning.csv -o ./Output/OTSG --stack-files
pipenv run python ./clone.py ./Replacements/OTSG.yaml ./Input/initialization_st.csv -o ./Output/OTSG --stack-files

#                            _
#  ___  __ _ _ __ ___  _ __ | | ___
# / __|/ _` | '_ ` _ \| '_ \| |/ _ \
# \__ \ (_| | | | | | | |_) | |  __/
# |___/\__,_|_| |_| |_| .__/|_|\___|
#                     |_|

# # Testing
# input=./Input/testInput.txt
# outputDir=Output/OTSG
# pipenv run python ./clone.py ./Replacements/OTSG.yaml $input -o ./Output/OTSG
# vim -d $input $outputDir/testInput9.txt

# Running
# ./clone.py ./Replacements/OTSG/dict_drawings.csv ./Input/drawing.txt -o Output/OTSG --latin1
# ./clone.py ./Replacements/OTSG/dict_gengraphics.csv ./Input/gengraphic.csv -o Output/OTSG --stack-files --gen-graphic
# ./clone.py ./Replacements/OTSG/dict_switches.csv ./Input/switches.csv -o Output/OTSG --stack-files
# ./clone.py ./Replacements/OTSG/dict_drawings.csv ./Input/tuning.csv -o Output/OTSG --stack-files
