"""
Test clone.py
"""

import os

import pytest
from cloning.clone import Clone


@pytest.fixture(scope='session')
def create_text_file():
    def _create_text_file(content):
        file_path = '/tmp/test'
        with open(file_path, 'w') as file:
            file.write(content)
        return file_path

    yield _create_text_file

    # Teardown: Delete the file after the tests
    os.remove('/tmp/test')


@pytest.mark.parametrize(
    'in_text, out_text',
    [
        # Pair 2: 2nd clone
        ('261FC03121', '261FC03171'),
        ('FC03121', 'FC03171'),
        ('261-F-03121', '261-F-03171'),
        ('261-FV-03121', '261-FV-03171'),
        ('FV-03121', 'FV-03171'),
        ('263L3P001T2', '263L3P002T2'),
        ('263II01901', '263II01902'),
        ('264-WP-101', '264-WP-102'),
        ('263WP101', '263WP102'),
        ('WP-101', 'WP-102'),
        ('WP101', 'WP102'),
        ('WI101', 'WI102'),
        ('P101', 'P102'),
        ('262HS201OOS', '262HS202OOS'),
        ('2641P01', '2641P02'),
        ('F264KV01106_OT', 'F264KV01156_OT'),
        ('II01901A', 'II01902A'),
        # Test combinations to make sure 2 or more regex substitutions aren't made
        ('WP-101\n261-FV-03121', 'WP-102\n261-FV-03171'),
        ('WP101\nP-101', 'WP102\nP-102'),
    ],
)
def test_well_pairs(monkeypatch, create_text_file, in_text, out_text):
    # Set up test file
    file_path = create_text_file(in_text)
    print('File path: ' + file_path)

    # Simulate command-line arguments
    monkeypatch.setattr(
        'sys.argv', ['clone.py', '/home/mike/git/Yokogawa/Cloning/replacements/WellPairs.yaml', file_path]
    )

    # Run the test
    Clone().run()
    file_path_clone = file_path.replace('test', 'output/test-2')
    with open(file_path_clone, 'r') as file:
        content = file.read()
        assert out_text in content


@pytest.mark.parametrize(
    'in_text, out_text, clone_number',
    [
        # 262-1: 2nd clone
        ('263FC01121', '262FC01121', 2),
        ('FC03121', 'FC01121', 2),
        ('261-F-03621', '262-F-01621', 2),
        ('262-FV-03121', '262-FV-01121', 2),
        ('FV-03121', 'FV-01121', 2),
        ('263L3P001T2', '262L3P001T1', 2),
        ('263II02901', '262II01901', 2),
        ('264-WP-301', '262-WP-101', 2),
        ('263WP201', '262WP101', 2),
        ('WP-201', 'WP-101', 2),
        ('WP301', 'WP101', 2),
        ('WI301', 'WI101', 2),
        ('P301', 'P101', 2),
        ('261HS211OOS', '262HS111OOS', 2),
        ('2613P01', '2621P01', 2),
        ('F261KV03306_OT', 'F262KV01306_OT', 2),
        # 266-2: 11th clone
        ('263FC01121', '266FC02121', 11),
        ('FC03121', 'FC02121', 11),
        ('261-F-03621', '266-F-02621', 11),
        ('262-FV-03121', '266-FV-02121', 11),
        ('FV-03121', 'FV-02121', 11),
        ('263L3P001T2', '266L3P001T2', 11),
        ('263II02901', '266II02901', 11),
        ('264-WP-301', '266-WP-201', 11),
        ('263WP201', '266WP201', 11),
        ('WP-101', 'WP-201', 11),
        ('WP301', 'WP201', 11),
        ('262HS211OOS', '266HS211OOS', 11),
        ('2613P01', '2662P01', 11),
        ('F261KV03306_OT', 'F266KV02306_OT', 11),
        # Test combinations to make sure 2 or more regex substitutions aren't made
        ('WP-201\n262-FV-03121', 'WP-101\n262-FV-01121', 2),
    ],
)
def test_well_pads(monkeypatch, create_text_file, in_text, out_text, clone_number):
    # Set up test file
    file_path = create_text_file(in_text)
    print('File path: ' + file_path)

    # Simulate command-line arguments
    monkeypatch.setattr(
        'sys.argv', ['clone.py', '/home/mike/git/Yokogawa/Cloning/replacements/WellPads.yaml', file_path]
    )

    # Run the test
    Clone().run()
    file_path_clone = file_path.replace('test', f'output/test-{clone_number}')
    with open(file_path_clone, 'r') as file:
        content = file.read()
        assert out_text in content
