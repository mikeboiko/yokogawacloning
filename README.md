# Overview

This script is used for cloning text files.
The entire codebase is contained in clone.py
Certain CLI arguments are specific to the Yokogawa Centum system.

## CLI Arguments

Run `python clone.py --help` to see all help details.

`--stack-files`: Instead of creating a new file for each set, create one concatenated file.
`--gen-graphic`: This will re-number the Centum generic graphic sets.

# YAML Syntax

## sets

Sets are used to name the output files.

Run `./clone.py dict.yaml file.txt`

```
sets:
  first: 1
  last: 3
  step: 1
```

3 files are created

```
.
├── file-1.txt
├── file-2.txt
├── file-3.txt
└── file.txt
```

## variables

The variables can either be constants, lists or dictionaries.
The lists are manual sets, while the dictionaries are numeric patterns.

### Constants

Example:

```
variables:
  x1: 10
```

### Lists

Example:

```
variables:
  x1:
    - 4
    - 9
    - 11
```

### Dictionaries

The dictionary sets contain to required keys:

- `start`: start at this number on the first set
- `step`: increment by this amount for each set

```
variables:
  x1:
    start: 1
    step: 2
```

## substitutions

Substitutions are key-value pairs where key is the search string and value is the replace string.

All substitutions accept python regex for key and value.

Substitution replace strings use variable names in the following manner:
`'01001': '{x1}001'`

In this example, the variable x1, is being referenced.

### standard:

standard substitutions should be used unless a python zfill() is required

```
substitutions:
  standard:
    'SG1(?!\d)': 'SG{x1}'
```

### zfill

zfill substitutions are defined with the key name, `zfill-N`, where N is the amount of zeroes python should `zfill()` with

For example, to get python to perform `zfill(2)`, define as follows:

```
substitutions:
  zfill-2:
    '01001': '{x1}001'
```

### lambda expressions

For advanced regex substitution, the replace string may also be in the form of a lambda expression as follows:

```
substitutions:
  standard:
    '26([1-6])': 'lambda m: "26" + m.group(1)'
```

When the replace string contains the keyword `lambda`, python runs `eval()` in `re.sub()`

# Examples

## Basic

Run `test.yaml file.txt --stack-files`, where:

Dictionary:

```
sets:
  first: 1
  last: 3
  step: 1
variables:
  x1:
    - 1
    - 2
    - 3
substitutions:
  standard:
    '01001': '0{x1}001'
```

`file.txt`:

```
205FC01001
```

Output of `file-stacked.txt`:

```
205FC01001
205FC02001
205FC03001
```

## Zfill

The previous example wouldn't work if there were 10 sets. In that case you could use zfill-2 substitutions as follows.

Run `test.yaml file.txt --stack-files`, where:

Dictionary:

```
sets:
  first: 1
  last: 10
  step: 1
variables:
  x1:
    start: 1
    step: 1
substitutions:
  zfill-2:
    '01001': '{x1}001'
```

`file.txt`:

```
205FC01001
```

Output of `file-stacked.txt`:

```
205FC01001
205FC02001
205FC03001
205FC04001
205FC05001
205FC06001
205FC07001
205FC08001
205FC09001
205FC10001
```

## Regex Back-References

Run `test.yaml file.txt --stack-files`, where:

Dictionary:

```
sets:
  first: 1
  last: 11
  step: 1
variables:
  x1:
    - 1
    - 2
    - 2
    - 2
    - 3
    - 3
    - 4
    - 4
    - 4
    - 5
    - 6
  x2:
    - 3
    - 1
    - 2
    - 3
    - 1
    - 2
    - 1
    - 2
    - 3
    - 2
    - 2
substitutions:
  standard:
    '26[1-6]([A-Za-z]+)0[1-3](\d{3})': '26{x1}\g<1>0{x2}\g<2>'
```

`file.txt`:

```
263LC02020A
```

Output of `file-stacked.txt`:

```
261LC03020A
262LC01020A
262LC02020A
262LC03020A
263LC01020A
263LC02020A
264LC01020A
264LC02020A
264LC03020A
265LC02020A
266LC02020A
```

## Lambda Expression

Run `test.yaml file.txt --stack-files`, where:

Dictionary:

```
sets:
  first: 1
  last: 11
  step: 1
variables:
  x1:
    start: 100
    step: 50
substitutions:
  standard:
    '26([1-6])([A-Za-z]+)0([1-3])1(\d{2})': 'lambda m: "26" + m.group(1) + m.group(2) + "0" + m.group(3) + str(int(m.group(4)) + {x1})'
```

`file.txt`:

```
263LC02120A
```

Output of `file-stacked.txt`:

```
263LC02120A
263LC02170A
263LC02220A
263LC02270A
263LC02320A
263LC02370A
263LC02420A
263LC02470A
263LC02520A
263LC02570A
263LC02620A
```
